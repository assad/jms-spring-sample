package persistence_sample.client;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * Author: Al-assad 余林颖
 * E-mail: yulinying_1994@outlook.com
 * Date: 2018/2/3 14:04
 * Description: topic 消息监听器，对接收到的消息进行处理
 */
public class TopicListener implements MessageListener {
    private static final Logger log = LogManager.getLogger();
    @Override
    public void onMessage(Message message) {
        try {
            String msg = ((TextMessage)message).getText();
            log.debug(msg);
        } catch (JMSException e) {
            e.printStackTrace();
        }

    }
}
