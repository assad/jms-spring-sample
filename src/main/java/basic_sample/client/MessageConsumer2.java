package basic_sample.client;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Author: Al-assad 余林颖
 * E-mail: yulinying_1994@outlook.com
 * Date: 2018/2/3 1:33
 * Description: ActiveMQ 消息队列消费端,测试 queue 和 topic 方式客户端接收的差异
 */
public class MessageConsumer2 {

    private static JmsTemplate jmsTemplate = null;
    private static final Logger log = LogManager.getLogger();

    public static void main(String[] args){
        jmsTemplate = new ClassPathXmlApplicationContext("basic_sample/appContext-client.xml").getBean("jmsTemplate",JmsTemplate.class);

        //启动3个线程模拟3个接收者同时接收队列信息，查看
        ExecutorService exec = Executors.newFixedThreadPool(3);
        for(int i=0; i< 3; i++){
            exec.execute(()->{
                while(true){
                    log.debug((String)jmsTemplate.receiveAndConvert());
                }
            });
        }
        exec.shutdown();
    }
}

/* output - queue mode:
01:44:07.405 [pool-2-thread-2] DEBUG basic_sample.client.MessageConsumer2 - Message@<2>
01:44:07.405 [pool-2-thread-1] DEBUG basic_sample.client.MessageConsumer2 - Message@<3>
01:44:07.405 [pool-2-thread-3] DEBUG basic_sample.client.MessageConsumer2 - Message@<1>
01:44:07.426 [pool-2-thread-2] DEBUG basic_sample.client.MessageConsumer2 - Message@<4>
01:44:07.806 [pool-2-thread-1] DEBUG basic_sample.client.MessageConsumer2 - Message@<5>
01:44:08.819 [pool-2-thread-3] DEBUG basic_sample.client.MessageConsumer2 - Message@<6>
01:44:09.833 [pool-2-thread-2] DEBUG basic_sample.client.MessageConsumer2 - Message@<7>
01:44:10.840 [pool-2-thread-1] DEBUG basic_sample.client.MessageConsumer2 - Message@<8>
01:44:11.855 [pool-2-thread-3] DEBUG basic_sample.client.MessageConsumer2 - Message@<9>
01:44:12.869 [pool-2-thread-2] DEBUG basic_sample.client.MessageConsumer2 - Message@<10>
01:44:13.886 [pool-2-thread-1] DEBUG basic_sample.client.MessageConsumer2 - Message@<11>
01:44:14.894 [pool-2-thread-3] DEBUG basic_sample.client.MessageConsumer2 - Message@<12>
01:44:15.906 [pool-2-thread-2] DEBUG basic_sample.client.MessageConsumer2 - Message@<13>
01:44:16.920 [pool-2-thread-1] DEBUG basic_sample.client.MessageConsumer2 - Message@<14>
01:44:17.935 [pool-2-thread-3] DEBUG basic_sample.client.MessageConsumer2 - Message@<15>
01:44:18.942 [pool-2-thread-2] DEBUG basic_sample.client.MessageConsumer2 - Message@<16>
01:44:19.966 [pool-2-thread-1] DEBUG basic_sample.client.MessageConsumer2 - Message@<17>
01:44:20.974 [pool-2-thread-3] DEBUG basic_sample.client.MessageConsumer2 - Message@<18>
01:44:21.983 [pool-2-thread-2] DEBUG basic_sample.client.MessageConsumer2 - Message@<19>
01:44:22.990 [pool-2-thread-1] DEBUG basic_sample.client.MessageConsumer2 - Message@<20>
* */

/* output - topic mode:
 01:47:45.466 [pool-2-thread-1] DEBUG basic_sample.client.MessageConsumer2 - Message@<1>
01:47:45.466 [pool-2-thread-2] DEBUG basic_sample.client.MessageConsumer2 - Message@<1>
01:47:45.466 [pool-2-thread-3] DEBUG basic_sample.client.MessageConsumer2 - Message@<1>
01:47:46.472 [pool-2-thread-3] DEBUG basic_sample.client.MessageConsumer2 - Message@<2>
01:47:46.472 [pool-2-thread-2] DEBUG basic_sample.client.MessageConsumer2 - Message@<2>
01:47:46.472 [pool-2-thread-1] DEBUG basic_sample.client.MessageConsumer2 - Message@<2>
01:47:47.476 [pool-2-thread-1] DEBUG basic_sample.client.MessageConsumer2 - Message@<3>
01:47:47.476 [pool-2-thread-2] DEBUG basic_sample.client.MessageConsumer2 - Message@<3>
01:47:47.476 [pool-2-thread-3] DEBUG basic_sample.client.MessageConsumer2 - Message@<3>
01:47:48.482 [pool-2-thread-3] DEBUG basic_sample.client.MessageConsumer2 - Message@<4>
01:47:48.482 [pool-2-thread-1] DEBUG basic_sample.client.MessageConsumer2 - Message@<4>
01:47:48.482 [pool-2-thread-2] DEBUG basic_sample.client.MessageConsumer2 - Message@<4>
01:47:49.491 [pool-2-thread-1] DEBUG basic_sample.client.MessageConsumer2 - Message@<5>
01:47:49.491 [pool-2-thread-3] DEBUG basic_sample.client.MessageConsumer2 - Message@<5>
01:47:49.491 [pool-2-thread-2] DEBUG basic_sample.client.MessageConsumer2 - Message@<5>
01:47:50.495 [pool-2-thread-2] DEBUG basic_sample.client.MessageConsumer2 - Message@<6>
01:47:50.495 [pool-2-thread-3] DEBUG basic_sample.client.MessageConsumer2 - Message@<6>
01:47:50.495 [pool-2-thread-1] DEBUG basic_sample.client.MessageConsumer2 - Message@<6>
01:47:51.499 [pool-2-thread-2] DEBUG basic_sample.client.MessageConsumer2 - Message@<7>
01:47:51.499 [pool-2-thread-1] DEBUG basic_sample.client.MessageConsumer2 - Message@<7>
01:47:51.499 [pool-2-thread-3] DEBUG basic_sample.client.MessageConsumer2 - Message@<7>
01:47:52.504 [pool-2-thread-2] DEBUG basic_sample.client.MessageConsumer2 - Message@<8>
01:47:52.504 [pool-2-thread-1] DEBUG basic_sample.client.MessageConsumer2 - Message@<8>
01:47:52.504 [pool-2-thread-3] DEBUG basic_sample.client.MessageConsumer2 - Message@<8>
01:47:53.509 [pool-2-thread-3] DEBUG basic_sample.client.MessageConsumer2 - Message@<9>
01:47:53.509 [pool-2-thread-2] DEBUG basic_sample.client.MessageConsumer2 - Message@<9>
01:47:53.509 [pool-2-thread-1] DEBUG basic_sample.client.MessageConsumer2 - Message@<9>
.......
* */