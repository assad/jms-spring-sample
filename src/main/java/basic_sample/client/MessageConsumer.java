package basic_sample.client;

import bean.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Author: Al-assad 余林颖
 * E-mail: yulinying_1994@outlook.com
 * Date: 2018/2/2 13:46
 * Description: ActiveMQ 消息队列消费端（同步方式），演示各种类型信息的接收
 */
public class MessageConsumer {

    private static JmsTemplate jmsTemplate = null;
    private static final Logger log = LogManager.getLogger();

    public static void main(String[] args){
        jmsTemplate = new ClassPathXmlApplicationContext("basic_sample/appContext-client.xml").getBean("jmsTemplate",JmsTemplate.class);

        //接收文本对象
        String msg = (String) jmsTemplate.receiveAndConvert();
        log.debug(msg);

        //接收 Map 对象
        Map<String,Integer> info = (HashMap<String,Integer>) jmsTemplate.receiveAndConvert();
        log.debug(info.keySet().stream().map(key -> key+":"+info.get(key)).collect(Collectors.joining(", ")));

        //接收 Object 类型对象
        User user = (User) jmsTemplate.receiveAndConvert();
        log.debug(user);

    }
}
