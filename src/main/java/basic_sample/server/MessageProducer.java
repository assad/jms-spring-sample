package basic_sample.server;

import bean.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Author: Al-assad 余林颖
 * E-mail: yulinying_1994@outlook.com
 * Date: 2018/2/2 13:45
 * Description: ActiveMQ 消息生产端，演示各种类型信息的发送
 */
public class MessageProducer {

    private static JmsTemplate jmsTemplate = null;
    private static final Logger log = LogManager.getLogger();

    public static void main(String[] args){
        jmsTemplate = new ClassPathXmlApplicationContext("basic_sample/appContext-server.xml").getBean("jmsTemplate",JmsTemplate.class);

        //发送文本信息
        String message = "basic_sample.server: Hello world!";
        jmsTemplate.convertAndSend(message);
        log.debug(message);

        //发送 Map 类型信息
        Map<String,Integer> info = new HashMap<>();
        info.put("Guangdong",64815);
        info.put("Jiangshu",62604);
        info.put("Shangdong",54868);
        info.put("Zhijiang",36958);
        jmsTemplate.convertAndSend(info);
        log.debug(info.keySet().stream().map(key -> key+":"+info.get(key)).collect(Collectors.joining(", ")));

        //发送 Object 类型信息
        User user = new User();
        user.setId(20137);
        user.setName("assad");
        user.setCity("Guangzhou");
        jmsTemplate.convertAndSend(user);
        log.debug(user);
    }
}
