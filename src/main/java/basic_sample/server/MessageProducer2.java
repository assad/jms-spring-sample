package basic_sample.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;

/**
 * Author: Al-assad 余林颖
 * E-mail: yulinying_1994@outlook.com
 * Date: 2018/2/3 1:28
 * Description: ActiveMQ 消息生产端，发送多条文本信息，测试 queue 和 topic 方式客户端接收的差异
 */
public class MessageProducer2 {

    private static JmsTemplate jmsTemplate ;
    private static final Logger log = LogManager.getLogger();

    public static void main(String[] args) throws InterruptedException {

        jmsTemplate = new ClassPathXmlApplicationContext("basic_sample/appContext-server.xml").getBean("jmsTemplate",JmsTemplate.class);
        //发送20条信息
        for(int i=1; i<=20; i++){
            String message = String.format("Message@<%d>",i);
            jmsTemplate.convertAndSend(message);
            log.debug(message);
            Thread.sleep(1000);
        }
    }
}
